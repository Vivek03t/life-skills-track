**Tiny Habits**

**Question 1:**
In this video, what was the most interesting story or idea for you?

**Answer:**
I found the story about the "Tiny Habits" concept itself to be the most interesting. It's fascinating how making small, manageable changes in our behavior can lead to significant and lasting improvements.

**Question 2:**
How can you use B = MAP to make making new habits easier? What are M, A, and P.

**Answer:**
To make creating new habits easier, you can use the formula B = MAP, where:

- **B:** Behavior - This is the habit one want to establish.
- **M:** Motivation - Finding a source of motivation or desire that will drive one to perform the behavior.
- **A:** Ability - Ensure that one has the ability to perform the behavior. Make it as simple and easy as possible.
- **P:** Prompt - Use a trigger or cue to remind self to perform the behavior at the right time.

**Question 3:**
Why is it important to "Shine" or Celebrate after each successful completion of a habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

**Answer:**
"Shining" or celebrating after successfully completing a habit is very crucial because it reinforces the habit loop. When one celebrates, the brain associates positive emotions with the habit, making it more likely to repeat in the future. Celebrations create a sense of accomplishment and satisfaction, which motivates one to continue the habit. Essentially, whatever one celebrates becomes a habit because it strengthens the neural pathways associated with that behavior.

**Question 4:**
In this video, what was the most interesting story or idea for you?

**Answer:**
The most interesting idea for me from this video was the concept of continuous improvement by getting 1% better every day. It highlights the power of small, consistent steps toward personal growth and development.

**Question 5:**
What is the book's perspective about Identity?

**Answer:**
The book "Atomic Habits" emphasizes the importance of identity in habit formation. According to the author, James Clear, your habits are not just what you do but also a reflection of who you believe you are. To make lasting changes, it's essential to align your identity with the desired habits. For example, if you want to become a runner, start by seeing yourself as a runner, and your habits will follow suit.

**Question 6:**
Write about the book's perspective on how to make a habit easier to do?

**Answer:**
The book suggests making a habit easier to do by reducing the friction associated with it. This can be achieved by:

- Making it visible: Place cues or reminders in your environment to make the habit more obvious.
- Starting with a small, easy step: Begin with a simplified version of the habit to build momentum.
- Designing your environment: Rearrange your surroundings to make the desired behavior more accessible.

**Question 7:**
Write about the book's perspective on how to make a habit harder to do?

**Answer:**
The book recommends making a habit harder to do when you want to break a bad habit or reduce its frequency. This can be done by:

- Increasing friction: Make the unwanted behavior more difficult by adding steps or obstacles.
- Reducing visibility: Remove cues or reminders associated with the habit.
- Designing your environment: Rearrange your surroundings to discourage the unwanted behavior.

**Reflection:**

**Question 8:**
Pick one habit that you would like to do more of? What are the steps that you can take to make the cue obvious or the habit more attractive or easy and/or response satisfying?

**Answer:**
I'd like to read more books. To make this habit more attractive and easy:

- I'll keep a book on my study table so it becomes habbit to read before bed.
- I'll create a cozy reading room to make the environment more attractive.
- I'll reward myself with a cup of tea after each reading session to make the response satisfying.

**Question 9:**
Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

**Answer:**
I want to reduce my time spent on social media and reading news excessively. To make this habit less attractive and hard:

- I'll turn off notifications to make the notifications less visible.
- I'll remove social media apps from my phone to make the process more challenging.
- I'll redirect my time to a more fulfilling hobby or activity to make the response unsatisfying.
