# Preventing Sexual Harassment

## Question 1
**What constitutes sexual harassment?**

- Sharing or displaying sexually explicit materials.
- Making sexually or gender-biased comments.
- Asking intrusive, personal questions of a sexual nature that can make the person uncomfortable.
- Staring at someone's body or subjecting them to sustained, unwanted attention that causes discomfort.

## Question 2
**How should you respond if you experience or witness such behavior repeatedly?**

1. Express discomfort to the person involved.
2. If the behavior persists, will report the issue to the supervisor or the appropriate authority for resolution.
