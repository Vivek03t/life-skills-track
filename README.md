# Life Skills Track

- [Learning Process](./learning-process.md)
- [Listening and Active Communication](./listening-and-assertive-communication.md)
- [Prevention of Sexual Harassment](./prevention-of-sexual-harassment.md)
- [Technical Paper  on OSI Layer](./technical-paper.md)
- [Energy Management](./energy-management.md)
- [Focus Management](./focus-%20management.md)
- [Grit and Growth Mindset](./grit-and-growth-mindset.md)
- [Tiny Habits](./tiny-habits.md)
- [Best  practices for software development](./best-practices-for-software-development.md)
  