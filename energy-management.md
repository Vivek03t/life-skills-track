# Energy Management

## Question 1: What activities help you relax in the Calm quadrant?

**Answer:** To achieve calmness, I enjoy listening to music, taking peaceful walks, talking to friends and family, and spending time in quiet places.

## Question 2: When do you find yourself entering the Stress quadrant?

**Answer:** I often find myself in the Stress quadrant when faced with a demanding workload that requires quick completion, making it challenging to maintain focus and productivity.

## Question 3: How do you recognize when you are in the Excitement quadrant?

**Answer:** I identify being in the Excitement quadrant when I'm filled with enthusiasm and positivity towards my work, and I'm eager to tackle challenges.

## Question 4: Summarize the Sleep is your Superpower video briefly, focusing on key points.

**Answer:** 
- The video emphasizes the significance of sleep for overall health.
- It highlights sleep's impact on memory and the immune system.
- Inadequate sleep can have detrimental effects.
- The video provides practical tips for improving sleep quality.

## Question 5: What are some strategies to enhance the quality of your sleep?

**Answer:**  
- Maintaining a consistent sleep schedule, going to bed and waking up at the same times daily.
- Engaging in relaxing activities before bedtime, such as reading or taking a warm bath.
- Ensuring your sleep environment is conducive with darkness, quiet, and comfort.
- Avoiding the use of electronic devices before bedtime.

## Question 6: Summarize the video - Brain Changing Benefits of Exercise, highlighting at least 5 key points.

**Answer:** 
- Exercise has a positive impact on brain health and structure.
- It contributes to increased happiness and reduced stress.
- Regular exercise can enhance memory and cognitive abilities.
- It facilitates the creation of new brain cells and promotes a healthy brain environment.
- Exercise is essential for maintaining mental sharpness.

## Question 7: What steps can you take to incorporate more physical activity into your routine?

**Answer:**
- Include simple stretching or basic exercises at home in your daily activities.
- Set achievable goals, like gradually increasing your daily walking distance.
- Make exercise a consistent part of your daily routine, even if it's only for a few minutes.
