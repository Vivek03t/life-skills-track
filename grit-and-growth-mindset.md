# Grit and Growth Mindset

## Question 1: Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

In video the point of discussion is the concept of grit and its importance in achieving long-term goals. It mentions that grit beats talent, IQ.

## Question 2: Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

In this video,the point of discussion is the idea of a growth mindset, emphasizing the belief that abilities can be developed through dedication and hard work.

## Question 3: What is the Internal Locus of Control? What is the key point in the video?

Internal Locus of Control means that the belief the individuals have control over their own outcomes and actions. The key point in the video is to understand and nurture the belief for motivation.

## Question 4: What are the key points mentioned by the speaker to build a growth mindset (explanation not needed).

Speaker mentions various key points to build a growth mindset.

- Praise for effort over innate ability leads to higher motivation and enjoyment.
- Developing an internal locus of control, believing in your ability to control your outcomes, is key for staying motivated.
- Problem-solving and recognizing your actions as the cause of improvements can reinforce an internal locus of control.
- An external locus of control can lead to less effort and motivation when individuals believe external factors control their success.
- Skillshare offers valuable courses, like productivity, to improve focus and enhance your internal locus of control, boosting motivation.


## Question 5: What are your ideas to take action and build a Growth Mindset?

My ideas to take action and build a growth mindset include:
  - Embracing challenges as opportunities to learn.
  - Seeking feedback and being open to criticism.
  - Setting specific, achievable goals.
  - Developing resilience in the face of setbacks.
  - Continuously expanding technical knowledge and skills.
