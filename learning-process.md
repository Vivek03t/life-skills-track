# Learning Process

### Question 1
What is the Feynman Technique? Explain in 1 line.

A technique to underestand a complicated concept and develop the deep learning tactics.


### Question 2
In this video, what was the most interesting story or idea for you?

Most interesting story for me was the Einstein's holding steel balls in his hands, while thinking and analyzing his experiments. Whenever he got sleep or a nap those steel balls fell from his hand and he wokeup to pick them up and in the process he get a short break and a way to resolve breakpoints. 

Most interesting ideas for me were -
- Go to diffuse mode whenever you are stuck
- Do Pomodoro technique.
- Do exercise
- Test yourself 
- Recall
- Practice
- Repeat

### Question 3
What are active and diffused modes of thinking?

Active mode of thinking means a state of a highly focused and concentrated mental effort.

Diffused mode of thinking means a relaxed, background thinking state where one is not exerting intense mental effort but thinking subconciously. Sometimes this can lead to unexpected insights and solutions emerging from nowhere.

### Question 4
According to the video, what are the steps to take when approaching a new topic? Only mention the points.

1. Deconstruct the skill
2. Learn enough to self-correct
3. Practice at least 20 hours
4. The major barrier to skill acquisition isn't intellectual, it's emotional.

### Question 5

What are some of the actions you can take going forward to improve your learning process?

1. Incorporate the Feynman technique into my learning path.
2. Teaching someone concepts learned.
3. Using Pomodoro technique.
4. Going to diffuse mode whenever stuck.
5. Practice at least 20 hours.
6. Exercise
7. Before starting to learn new things i will deconstruct it.

